import {formRegistr} from './forms/formRegist.js';
import {formEntry} from './forms/formEntry.js';
import {createTag} from './helper.js';

export function content() {
    const mainContent = createTag('main', {class: 'main_content'});
    mainContent.prepend(formRegistr(), formEntry());
    return mainContent;
}