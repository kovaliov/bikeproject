import {createTag} from './helper.js';

export function footer() {
    const footer = createTag(
        'footer',
        {class: 'wrapper_footer'},
        '',
        [
            ['div', {class: 'copyright'}, 'Сайт разработал Ковалёв Юрий Сергеевич']
        ]
    )
    return footer;
}