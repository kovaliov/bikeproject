import {createTag} from '../helper.js';

export function formEntry() {
  const form = createTag('form', {class: 'form form_entry authorization'});
  form.prepend(
    createTag('button', {type: 'button', class: 'close'}, '×'),
    createTag('h2', {class: 'h_2'}, 'Вход'),
    createTag(
      'div',
      {class: 'password'},
      '',
      [
        ['input', {class: 'input', type: 'text', placeholder: 'Логин'}]
      ]  
    ),
    createTag(
      'div', 
      {class: 'password'},
      '',
      [
        ['input', {type: 'password', class: 'input', placeholder: 'Пароль', name: 'password'}], 
        ['button', {type: 'button', class: 'password_control', tabindex: '-1'}],
        ['p', {class: 'error error_entry'}]
      ]
    ),
    createTag(
      'div',
      {class: 'button_form'},
      '',
      [
        ['button', {type: 'button', class: 'button ok', tabindex: '0'}, 'OK']
      ]
    )
  )
  return form;
}
