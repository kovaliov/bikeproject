import {createTag} from '../helper.js';

export function formRegistr() {
  const form = createTag('form', {class: 'form form_regist authorization', name: 'regist'});
  form.prepend(
    createTag('button', {type: 'button', class: 'close'}, '×'),
    createTag('h2', {class: 'h_2'}, 'Регистрация'),
    createTag(
      'div',
      {class: 'password'},
      '',
      [
        ['input', {class: 'input', type: 'text', placeholder: 'Фамилия'}],
        ['p', {class: 'error '}, 'Введите фамилию']
      ]
    ),
    createTag(
      'div',
      {class: 'password'},
      '',
      [
        ['input', {class: 'input', type: 'text', placeholder: 'Имя'}],
        ['p', {class: 'error '}, 'Введите имя']
      ]
    ),
    createTag(
      'div',
      {class: 'password'},
      '',
      [
        ['input', {class: 'input', type: 'text', placeholder: 'Логин'}],
        ['p', {class: 'error '}, 'Введите логин']
      ]
    ),
    createTag(
      'div', 
      {class: 'password'},
      '',
      [
        ['input', {type: 'password', class: `input password1`, placeholder: 'Пароль'}],
        ['button', {type: 'button', class: 'password_control', tabindex: '-1'}],
        ['p', {class: 'error error_regist'}, 'Введите пароль']
      ]
    ),
    createTag(
      'div',
      {class: 'password'},
      '',
      [
        ['input', {type: 'password', class: `input password2`, placeholder: 'Подтвердите пароль'}],
        ['button', {type: 'button', class: 'password_control', tabindex: '-1'}],
        ['p', {class: 'error error_regist'}, 'Введите пароль']
      ]
    ),
    createTag(
      'div',
      {class: 'password'},
      '',
      [
        ['input', {class: 'input', type: 'email', placeholder: 'Email'}],
        ['p', {class: 'error error_regist'}, 'Введите email']
      ]
    ),
    createTag(
      'div',
      {class: 'password'},
      '',
      [
        ['input', {class: 'input', type: 'tel', placeholder: '+375 99 999 99 99'}],
        ['p', {class: 'error error_regist'}, 'Введите телефон']
      ]
    ),
    createTag(
      'div',
      {class: 'button_form'},
      '',
      [
        ['button', {type: 'button', class: 'button ok'}, 'Ok'],
        ['button', {type: 'reset', class: 'button'}, 'Сбросить']
      ]
    )
  )
  return form;
}






