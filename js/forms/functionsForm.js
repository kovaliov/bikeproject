function inActivateElement(el) {
  el.classList.remove('active');
}

function activateElement(el) {
  el.classList.add('active');
}

function showForm(type) {
  const formElement = document.querySelector(`.form_${type}`);
  formElement.classList.add('opened');
}

function closeForm(formEl) {
  formEl.classList.remove('opened');
}

export function closeForms() {
  document.querySelectorAll('.form.authorization').forEach(el => closeForm(el));
  document.querySelectorAll('.auth .button').forEach(el => inActivateElement(el));
  clearValue();
}

// -------------- кнопки открытия форм----------------------------------------

export function openForm(event) {
  const formType = event.target.classList.contains('entry') ? 'entry': 'regist';
  event.stopPropagation();
  closeForms();
  activateElement(event.target);
  showForm(formType);
}

// ------------- кнопка Х  и ESC -------------------------------------------

export function closeFormKey(event) {
  if(event.code === 'Escape') {
    closeForms();
  }
}

// ------------- кнопка ОК регистрации -------------------------------------------

export function submitForm() {
  const isValid = validate();
  if (isValid) {
    console.log('localStorage');
    localStorage.setItem('key', 'value');
    closeForms();
  } 
}

function validate() {
  const formRegist = document.forms.regist;
  const inputs = document.querySelectorAll('.form.opened .input');
  let isValid = true;
  inputs.forEach(el => {
    const errorEl = el.parentElement.querySelector('.error');
    if (!el.value.trim()) {
      
      errorEl.classList.add('opened');
      isValid = false;
    } else {
      errorEl.classList.remove('opened');
    }
  });

  const valuePass1 = formRegist.querySelector('.password1').value;
  const valuePass2 = formRegist.querySelector('.password2').value;
  const errorRegist = formRegist.querySelector('.error_regist');

  if(valuePass1 != valuePass2) {
    errorRegist.innerHTML = 'Пароли не совпадают!!!';
    errorRegist.classList.add('opened');
    isValid = false;
  }
  return isValid;
}

function clearValue() {
  const values = document.querySelectorAll('.input');
  const parags = document.querySelectorAll('.error');
  parags.forEach(item => {
    item.classList.remove('opened');
  });
  values.forEach(item => {
    item.value = '';
  });
}

