import {createTag} from './helper.js';

export function header() {
   const header = createTag('header', {class: 'header'});
   const main = createTag('div', {class: 'main_nav'});
   header.prepend(main);
   main.prepend(
      createTag(
         'div',
         {class: 'logo'},
         '',
         [
            ['span', {class: 'span_first'}, 'Вело'],
            ['span', {class: 'span_second'}, 'Мир']
         ]
      ),
      /*createTag(
         'nav',
         {class: 'nav'},
         '',
         [
            ['a', {href: '#', class: `nav_item button`}, 'Главная'],
            ['a', {href: '#', class: `nav_item button`}, 'Контакты'],
            ['a', {href: '#', class: `nav_item button`}, 'О нас']
         ]
      ),*/
      createTag(
         'div',
         {class: 'auth'},
         '',
         [
            ['button', {type: 'button', class: `button entry`}, 'Вход'],
            ['button', {type: 'button', class: `button regist`}, 'Регистрация']
         ]
      )
   )
   return header;
}





