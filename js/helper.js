export function createTag(elem, attrs, text, involve) {
    const tag = document.createElement(elem);
    Object.entries(attrs).forEach(([key, value]) => {
        tag.setAttribute(key, value);
    });
    text ? tag.innerText = text: tag.innerText = '';
    if(involve) {
      involve.forEach(([elm, att, txt]) => {
        const tagChild = document.createElement(elm);
        Object.entries(att).forEach(([k, v]) => {
          tagChild.setAttribute(k, v);
          txt ? tagChild.innerText = txt: tagChild.innerText = '';
          tag.append(tagChild);
        });
      });
    }
    return tag;
  }
