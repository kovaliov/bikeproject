import {header} from './header.js';
import {content} from './content.js';
import {footer} from './footer.js';
import {openForm, closeFormKey, closeForms, submitForm} from './forms/functionsForm.js';

let body = document.getElementById('root');
body.append(header(), content(), footer());

// -------------- кнопки открытия форм----------------------------------------

const authorizationButtons = document.querySelectorAll('.auth .button');

authorizationButtons.forEach(el => {
   el.addEventListener('click', openForm);
});

// ------------- кнопка Х  и ESC -------------------------------------------

const closeButtons = document.querySelectorAll('.authorization .close');
closeButtons.forEach(el => {
   el.addEventListener('click', closeForms);
});

document.addEventListener('keydown', closeFormKey);

// --------------------- add submit handler -----------------------------------
const submitButtons = document.querySelectorAll('.authorization .ok');
submitButtons.forEach(el => {
   el.addEventListener('click', submitForm);
});

// -------------- кнопка глаз ------------------------------------------
const passwords = document.querySelectorAll('.password');

passwords.forEach((password) => {
   password.addEventListener('click', () => {
      if(event.target.className === 'password_control') {
         password.children[0].setAttribute('type', 'text');
         event.target.classList.toggle('view');
      } else if(event.target.className === 'password_control view') {
         password.children[0].setAttribute('type', 'password');
         event.target.classList.toggle('view');
         }
   }, true)
})

